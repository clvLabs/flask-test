# flask test

## Build container
```bash
#!/bin/bash
docker build \
  -t flask-test \
  ~/src/python/test/flask
```

## Dev run
```bash
#!/bin/bash
docker run \
  -it \
  --rm \
  --volume ~/src/python/test/flask/app:/app \
  --publish 8001:80 \
  --name dev-flask-test \
  -e SECRET_KEY="some-secret-key" \
  flask-test

```