FROM python:3

RUN apt update
RUN apt -y upgrade

RUN pip3 install flask flask-wtf
COPY app /app
WORKDIR /app

EXPOSE 80/tcp

ENV FLASK_APP="/app/app.py"
ENV FLASK_ENV=development
ENV SECRET_KEY=pleaseOverrideMe

ENTRYPOINT ["flask", "run", "--host=0.0.0.0", "--port=80"]
