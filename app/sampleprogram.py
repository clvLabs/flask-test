import sys
import time

def dothings(num):
  for a in range(num):
    print('.', end='')
    time.sleep(0.1)
  print()

print("sampleprogram")
print("args = ", sys.argv)
print("Doing some things...")
dothings(10)
print("Doing some more things...")
dothings(15)
print("Doing some other things...")
dothings(20)

print("FINISHED!")
