from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired

class ParamsForm(FlaskForm):
    champ = StringField('Championship', validators=[DataRequired()])
    event = StringField('Event', validators=[DataRequired()])
    session = StringField('Session', validators=[DataRequired()])
    maxriders = StringField('Max Riders')
    submit = SubmitField('Run')
