from app import app
import subprocess
import time
from flask import Flask, request, flash, redirect, render_template, Response
from app.forms import ParamsForm
from pprint import pprint

@app.route('/', methods=['GET', 'POST'])
def params():
  form = ParamsForm()
  if form.validate_on_submit():
    def inner():
      proc = subprocess.Popen(
        # ['ls', '-la'],
        # ['dmesg'],
        [
          'python3',
          '-u',
          'sampleprogram.py',
          form.champ.data,
          form.event.data,
          form.session.data,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT
      )

      for line in iter(proc.stdout.readline, None):
        if not line:
          break
        yield line.rstrip().decode('utf8') + '<br/>\n'

      yield '<a href="/somefile">Download somefile</a><br/>\n'
      yield '<a href="/">HOME</a><br/>\n'

    return Response(inner(), mimetype='text/html')
      # flash('Request received: {} {} {}'.format(
      #     form.champ.data, form.event.data, form.session.data))
      # return redirect('/')

  return render_template(
    'paramsform.html',
    form=form,
    title='dtvstats',
    statname='average-lap-time' )


@app.route('/count')
def count():
    def inner():
        for x in range(100, 0, -1):
            time.sleep(0.1)
            yield '%s - \n' % x
    return Response(inner(), mimetype='text/html')  # text/html is required for most browsers to show the partial page immediately


@app.route('/test')
def test():
  def inner():

    proc = subprocess.Popen(
      # ['ls', '-la'],
      # ['dmesg'],
      ['python3', '-u', 'sampleprogram.py'],
      stdout=subprocess.PIPE,
      stderr=subprocess.STDOUT
    )

    for line in iter(proc.stdout.readline, None):
      if not line:
        break
      yield line.rstrip().decode('utf8') + '<br/>\n'

  return Response(inner(), mimetype='text/html')
